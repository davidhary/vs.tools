# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [1.0.8940] - 2024-06-23 Preview 202304
* Update to .Net 8.
* Apply code analysis rules.

## [1.0.8523] - 2023-05-02
* Split README.MD to attribution, cloning, open-source and read me files.
* Add code of conduct, contribution and security documents.

## [1.0.8508] - 2023-04-17
* Use cc.isr namespace prefix.

## [1.0.8104] - 2022-03-10
* Forked from [dn.core].

&copy; 2012 Integrated Scientific Resources, Inc. All rights reserved.

[dn.core]: https://www.bitbucket.org/davidhary/dn.core
[1.0.9083]: https://bitbucket.org/davidhary/vs.tools/src/main/
