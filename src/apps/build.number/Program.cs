using System;

namespace cc.isr.Tools;

/// <summary> A program. </summary>
/// <remarks> David, 2020-09-30. </remarks>
public sealed class Program
{
    /// <summary>
    /// Provides <see cref="Microsoft.Win32.RegistryKey" /> objects that represent the root keys in
    /// the Windows registry, and <see langword="static" /> methods to access key/value pairs.
    /// </summary>
    /// <remarks> David, 2020-09-30. </remarks>
    private Program()
    { }

    /// <summary> Main entry-point for this application. </summary>
    /// <remarks> David, 2020-09-30. </remarks>
    [STAThread]
    internal static void Main()
    {
        string? dateEntered = DateTimeOffset.UtcNow.ToString( "d", System.Globalization.CultureInfo.CurrentCulture );
        Console.WriteLine( $"UTC {dateEntered} Build Number: {CountDaysSinceMillennium( dateEntered )}" );
        do
        {
            Console.WriteLine( $"Enter UTC build date, e.g., {dateEntered}, or Enter to exit: " );
            dateEntered = Console.ReadLine();
            if ( dateEntered?.Length > 0 )
            {
                try
                {
                    Console.WriteLine( $"UTC {dateEntered} Build Number: {CountDaysSinceMillennium( dateEntered )}" );
                }
                catch ( Exception ex )
                {
                    Console.WriteLine( ex.ToString() );
                }
            }
        }
        while ( dateEntered?.Length != 0 );
    }

    /// <summary> Count days since millennium. </summary>
    /// <remarks> David, 2020-09-30. </remarks>
    /// <param name="revisionDate"> The revision date. </param>
    /// <returns> The total number of days since millennium. </returns>
    private static int CountDaysSinceMillennium( DateTimeOffset revisionDate )
    {
        return Convert.ToInt32( Math.Floor( revisionDate.UtcDateTime.Subtract( DateTime.Parse( "2000-01-01", System.Globalization.CultureInfo.CurrentCulture ) ).TotalDays ) );
    }

    /// <summary> Count days since millennium. </summary>
    /// <remarks> David, 2020-09-30. </remarks>
    /// <param name="revisionDate"> The revision date. </param>
    /// <returns> The total number of days since millennium. </returns>
    private static int CountDaysSinceMillennium( string revisionDate )
    {
        return CountDaysSinceMillennium( DateTimeOffset.Parse( revisionDate, System.Globalization.CultureInfo.CurrentCulture ) );
    }
}
