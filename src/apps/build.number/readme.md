# About

_BuildNumber_ is a .Net console application for 
calculating the build number based on the date since 2000-01-01.

# How to Use

Run the program from the command console.

# Key Features

* Calculates the build number for the current date;
* Calculates the build number for a given date.

# Main Types

The main types provided by this library are:

* _n/a_ 

# Feedback

_BuildNumber_ is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Tools Repository].

[Tools Repository]: https://bitbucket.org/davidhary/vs.tools

